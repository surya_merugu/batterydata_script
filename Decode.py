import sys
import os
import io
from komodo_py import *

bitrate  = 500000
can_data = array('B', [0]*8)
port     = 0
power    = KM_TARGET_POWER_ON
info      = km_can_info_t()
pkt       = km_can_packet_t()

def async_send (km, can_id, can_ch, iterate):
    arb_count = 0
    timeout   = 500
    data      = array('B', can_data)

    #pkt       = km_can_packet_t()
    #pkt.dlc   = len(data)
    pkt.id    = can_id

    # Send first packet
    km_can_async_submit(km, can_ch, 0, pkt, data)
    print('Submitted packet 1')

    for i in range(iterate - 1):
        # Send another packet while the first is still in progress
        km_can_async_submit(km, can_ch, 0, pkt, data)
        print('Submitted packet %d' % (i + 2))

        # The application can now perform other functions
        # while the Komodo is sending the previous packet
        km_sleep_ms(100);

        # Collect response to previously submitted packet
        (ret, arb_count) = km_can_async_collect(km, timeout)

        if (ret == KM_OK):
            print('Collected packet %d' % (i + 1))
        else:
            print('Error %d' % ret)

    # Collect the last response
    (ret, arb_count) = km_can_async_collect(km, timeout)
    if (ret == KM_OK):
        print('Collected packet %d\n' % (iterate))
    else:
        print('Error %d\n' % ret)

# Open the interface
km = km_open(port)
if km <= 0:
    print('Unable to open Komodo on port %d' % port)
    print('Error code = %d' % km)
    sys.exit(1)

# Acquire features
ret = km_acquire(km, KM_FEATURE_CAN_A_CONFIG | KM_FEATURE_CAN_A_CONTROL | KM_FEATURE_CAN_A_LISTEN)
print('Acquired features', hex(ret))

# Set bitrate
ret = km_can_bitrate(km, KM_CAN_CH_A, bitrate)
print('Bitrate set to %d kHz' % (ret // 1000))

# Set target power
km_can_target_power(km, KM_CAN_CH_A, power);
print('Target power %s' %('ON' if power else 'OFF'))
print()

# Enable Komodo
ret = km_enable(km)
if (ret != KM_OK):
    print('Unable to enable Komodo')

#Requesting for Battery pack Physical ID
can_id    = 0x00C30004
pkt.dlc   = 0
async_send(km, can_id, KM_CAN_CH_A, 1)


ret = 0
while(ret == 0):
    (ret, info, pkt, can_data)= km_can_read(km, can_data)
PhyID = [0]  * 7
PhyID[0] = chr(can_data[0])
PhyID[1] = chr(can_data[1])
PhyID[2] = chr(can_data[2])
PhyID[3] = chr(can_data[3])
PhyID[4] = chr(can_data[4])
PhyID[5] = chr(can_data[5])
PhyID[6] = chr(can_data[6])
print('Physical ID : ', end='')
out = io.StringIO()
out.writelines(PhyID)
print('RACE' + out.getvalue())

#Requesting for Battery FW Version
can_id    = 0x00C30002
pkt.dlc   = 0
async_send(km, can_id, KM_CAN_CH_A, 1)

ret = 0
while(ret == 0):
    (ret, info, pkt, can_data)= km_can_read(km, can_data)
FW_Ver = [0]  * 2
FW_Ver[0] = hex(can_data[0])
FW_Ver[1] = hex(can_data[1])
print('Firmware version : ', end='')
print(FW_Ver)

#Requesting for Battery CAN ID
can_id    = 0x00C30003
pkt.dlc   = 0
async_send(km, can_id, KM_CAN_CH_A, 1)

ret = 0
while(ret == 0):
    (ret, info, pkt, can_data)= km_can_read(km, can_data)
CanId_Buf = [0]  * 3
CanId_Buf[0] = int(can_data[0])
CanId_Buf[1] = int(can_data[1])
CanId_Buf[2] = int(can_data[2])
CanId = ((CanId_Buf[0] << 16) | (CanId_Buf[1] << 8) | CanId_Buf[2])
print('CAN ID : ', end='')
print(hex(CanId))

#Requesting for Battery Location
can_id    = 0x00C30005
pkt.dlc   = 0
async_send(km, can_id, KM_CAN_CH_A, 1)

ret = 0
while(ret == 0):
    (ret, info, pkt, can_data)= km_can_read(km, can_data)
Location = hex(can_data[0])
print('Pack Location : ', end='')
print(Location)

#Requesting for Battery RCPS Mode
can_id    = 0x00C30006
pkt.dlc   = 0
async_send(km, can_id, KM_CAN_CH_A, 1)

ret = 0
while(ret == 0):
    (ret, info, pkt, can_data)= km_can_read(km, can_data)
RCPS = hex(can_data[0])
print('RCPS Mode : ', end='')
print(RCPS)
